<?php

namespace App\Services\ProductExporter;

use App\Models\Product;
use App\Models\ProductExportTask;
use App\Services\ProductExporter\Jobs\ProductExport;
use Illuminate\Support\Facades\Bus;

class ProductExporter
{
    private ProductExportTask $productExportTask;
    private Product $model;

    public function __construct(Product $product, ProductExportTask $productExportTask)
    {
        $this->productExportTask = $productExportTask;
        $this->model = $product;
    }

    /**
     * Run Product Exporter
     *
     * @param int $limit
     * @throws \Throwable
     */
    public function run(int $limit = 100)
    {
        $this->model->query()
            ->select(['id'])
            ->chunkById($limit, function ($products) {

                $productJobs = $products->mapInto(ProductExport::class);
                $batch = Bus::batch($productJobs)
                    ->allowFailures()
                    ->dispatch();
                $this->saveExportTask($batch->id);
            });
    }

    /**
     * Save Export Task to DB
     *
     * @param string $batchId
     */
    private function saveExportTask(string $batchId): void
    {
        $this->productExportTask->query()->create([
            'batch_id' => $batchId
        ]);
    }
}
