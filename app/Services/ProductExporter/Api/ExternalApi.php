<?php

namespace App\Services\ProductExporter\Api;

use App\Models\Product;
use App\Services\ProductExporter\Api\Transformers\ProductTransformer;

class ExternalApi extends ExternalApiAbstract
{
    /**
     * Export product to the REST API.
     *
     * @param Product $product
     * @return bool
     */
    public function exportProduct(Product $product): bool
    {
        $productTransformed = (new ProductTransformer)->transform($product);
        $response = $this->httpClient->post('product', $productTransformed);

        return $response->successful();
    }
}
