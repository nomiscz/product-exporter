<?php

namespace App\Services\ProductExporter\Api;

use App\Models\Product;

interface ExternalApiInterface
{
    /**
     * Export product to the REST API.
     *
     * @param Product $product
     * @return bool
     */
    public function exportProduct(Product $product): bool;
}
