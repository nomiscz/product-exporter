<?php

namespace App\Services\ProductExporter\Api;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;

abstract class ExternalApiAbstract implements ExternalApiInterface
{
    protected string $apiEndpoint = 'https://api.domain.com';
    protected string $apiVersion = 'v1';

    protected array $config;
    protected PendingRequest $httpClient;

    public function __construct()
    {
        $this->config = config('services.product_exporter');
        $this->httpClient = Http::baseUrl($this->getApiUri())
            ->withToken($this->getApiToken());
    }

    /**
     * Get API URI
     *
     * @return string
     */
    private function getApiUri(): string
    {
        return "$this->apiEndpoint/$this->apiVersion/";
    }

    /**
     * Get API Token
     *
     * @return string
     */
    private function getApiToken(): string
    {
        return $this->config->get('token');
    }
}
