<?php

namespace App\Services\ProductExporter;

use Illuminate\Support\ServiceProvider;

class ProductExporterServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ProductExporter::class, function ($app) {
            return new ProductExporter();
        });

        $this->app->alias(ProductExporter::class, 'product.exporter');
    }
}
